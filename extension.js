/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
import GObject from 'gi://GObject';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import * as KeyboardMisc from 'resource:///org/gnome/shell/misc/keyboardManager.js'

import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';
import {QuickToggle, SystemIndicator} from 'resource:///org/gnome/shell/ui/quickSettings.js';

const ExampleToggle = GObject.registerClass(
class ExampleToggle extends QuickToggle {
    constructor() {
        super({
            title: _('Open OSK'),
            iconName: 'input-keyboard-symbolic',
            toggleMode: true,
        });
    }
});

const ExampleIndicator = GObject.registerClass(
class ExampleIndicator extends SystemIndicator {
    constructor() {
        super();

        this._indicator = this._addIndicator();
        this._indicator.iconName = 'face-smile-symbolic';

        const toggle = new ExampleToggle();
        //toggle.bind_property('checked',
        //    this._indicator, 'visible',
        //    GObject.BindingFlags.SYNC_CREATE);
        var keyboard_close = null;
        toggle.connect('notify::checked', (ttt)=>{
            if(ttt.checked == true){
                this.turn_on_osk();
            }
            else{
                this.turn_off_osk();
            }
        });
        this.quickSettingsItems.push(toggle);
    }
    turn_on_osk(){
        Main.keyboard.keyboardActor.open(true);
        KeyboardMisc.holdKeyboard();
        this.keyboard_close = Main.keyboard.keyboardActor.close;
        Main.keyboard.keyboardActor.close = (imm)=>{
            console.log("NOSK: Close prevented: close("+imm+") redirrected");
        }
    }
    turn_off_osk(){
        console.log("NOSK: keyboard_close present?"+this.keyboard_close == null);
        if(this.keyboard_close)
            Main.keyboard.keyboardActor.close = this.keyboard_close;
        KeyboardMisc.releaseKeyboard();
        Main.keyboard.keyboardActor.close(true);
    }
});

export default class QuickSettingsExampleExtension extends Extension {
    enable() {
        this._indicator = new ExampleIndicator();
        Main.panel.statusArea.quickSettings.addExternalIndicator(this._indicator);
    }

    disable() {
        this._indicator.turn_off_osk();
        this._indicator.quickSettingsItems.forEach(item => item.destroy());
        this._indicator.destroy();
    }
}
